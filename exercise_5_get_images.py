import boto3
from operator import itemgetter

ecr_client = boto3.client('ecr')

repositories = ecr_client.describe_repositories()

counter = 1
for repo in repositories['repositories']:
    images = ecr_client.describe_images(
        repositoryName=repo['repositoryName']
    )

    images_sorted_by_date = sorted(images['imageDetails'], key=itemgetter('imagePushedAt'), reverse=True)

    for image in images_sorted_by_date:
        print(f"{image['imageTags'][0]}")
