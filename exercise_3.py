import boto3
import time
import paramiko
import schedule
import requests

ec2_client = boto3.client('ec2', region_name="eu-west-3")
ec2_resource = boto3.resource('ec2', region_name="eu-west-3")


def are_instances_on(min_count):
    while True:
        # Retrieve and save the instances info into instances variable
        statuses = ec2_client.describe_instance_status()

        counter = 0
        is_fully_initialized = True
        for status in statuses['InstanceStatuses']:
            ins_status = status['InstanceStatus']['Status']
            sys_status = status['SystemStatus']['Status']
            ins_state = status['InstanceState']['Name']
            print(f"Instance {status['InstanceId']} is {ins_state}, status is {ins_status} and system status is {sys_status}.")

            if ins_status != 'ok' or sys_status != 'ok' or ins_state != 'running':
                is_fully_initialized = False

            counter += 1

        # Adds a pad of 5 seconds
        time.sleep(5)

        # The loop breaks only if all the instances are fully initialized
        if is_fully_initialized and counter == min_count:
            break

    print('Instances fully initialized!')


def create_instances(sg_id):
    min_count = 2
    instances = ec2_resource.create_instances(
        ImageId='ami-0b8b5288592eca360',
        InstanceType='t2.micro',
        MinCount=min_count,
        MaxCount=2,
        TagSpecifications=[
            {
                'ResourceType': 'instance',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': 'myapp'
                    },
                ]
            },
        ],
        KeyName='myapp-key-pair',
        SecurityGroupIds=[
            sg_id,
        ],
    )

    print("Creating instances...")

    # Verify the instances are fully initialized
    are_instances_on(min_count)


# The security group function receives a personal ip and creates the following rules:
# Port 22 for ssh connections
# Port 8080 for browser
def create_security_group(personal_ip):
    sg = ec2_resource.create_security_group(
        Description='Security Group for myapp instance',
        GroupName='myapp-sg',
    )

    # Gets the security id and modify the necessary data
    security_group = ec2_resource.SecurityGroup(sg.id)

    # Adds a name to the security group
    security_group.create_tags(
        Tags=[
            {
                'Key': 'Name',
                'Value': 'myapp-sg'
            },
        ]
    )

    # Add Inbound Rules
    security_group.authorize_ingress(
        IpPermissions=[
            {
                'FromPort': 22,
                'IpProtocol': 'tcp',
                'IpRanges': [
                    {
                        'CidrIp': f'{personal_ip}/32',
                        'Description': 'SSH rule'
                    },
                ],
                'ToPort': 22,
            },
            {
                'FromPort': 8080,
                'IpProtocol': 'tcp',
                'IpRanges': [
                    {
                        'CidrIp': f'{personal_ip}/32',
                        'Description': 'HTTP rule'
                    },
                ],
                'ToPort': 8080,
            },
        ],
    )

    return sg.id


def initialize_instance():
    # Retrieve the instances data in order to get their IPs
    reservations = ec2_client.describe_instances()

    for reservation in reservations['Reservations']:
        instances = reservation['Instances']

        for instance in instances:
            if instance['State']['Name'] == 'running':
                print(f"Connecting to Instance (ip: {instance['PublicIpAddress']})")
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
                ssh.connect(instance['PublicIpAddress'], 22, username="ec2-user", key_filename="myapp-key-pair.pem")

                # Installing Docker
                print('Updating OS')
                stdin, stdout, stderr = ssh.exec_command('sudo yum update')
                print(f"Result: {stdout.readlines()}")
                print("\nInstalling Docker")
                stdin, stdout, stderr = ssh.exec_command('sudo yum install docker')
                stdin.write('y\n')
                stdin.flush()
                print(f"Result: {stdout.readlines()}")
                print("\nStarting docker service...")
                stdin, stdout, stderr = ssh.exec_command('sudo service docker start')
                print(f"Result: {stdout.readlines()}")
                print("\nRunning nginx container...")
                stdin, stdout, stderr = ssh.exec_command('sudo docker run -d -p 8080:80 nginx')
                print(f"Result: {stdout.readlines()}")

                print("Closing connection...")
                ssh.close()


def monitor_apps():
    # Retrieve the instances data in order to get their IPs
    reservations = ec2_client.describe_instances()

    for reservation in reservations['Reservations']:
        instances = reservation['Instances']

        counter = 0
        for instance in instances:
            if instance['State']['Name'] == 'running':
                while True:
                    res = monitor_nginx(instance, 22, "ec2-user", "myapp-key-pair.pem")

                    if res == "success":
                        break

                    if res == "error:container" or "error:server":
                        counter += 1
                        time.sleep(5)

                    if counter == 5:
                        restart_container(instance, 22, "ec2-user", "myapp-key-pair.pem")
                        break


def restart_container(instance, port, username='ec2-user', key_filename="~/.ssh/id_rsa"):
    # Restart the app
    print('Restarting the app...')
    instance_public_ip = instance['PublicIpAddress']

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.connect(instance_public_ip, port, username=username, key_filename=key_filename)
    stdin, stdout, stderr = ssh.exec_command('sudo docker start $(sudo docker ps -aqf "ancestor=nginx")')
    print(f"Stdout: {stdout.readlines()}")
    ssh.close()
    print('Application restarted!')


def monitor_nginx(instance, port, username='ec2-user', key_filename="~/.ssh/id_rsa"):
    try:
        instance_public_ip = instance['PublicIpAddress']
        instance_public_dns = instance['PublicDnsName']
        print(f"Connecting to Instance (ip: {instance_public_ip})...")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
        ssh.connect(instance_public_ip, port, username=username, key_filename=key_filename)
        msg = "success"

        response = requests.get(f'http://{instance_public_dns}:8080')
        if response.status_code == 200:
            print('Website running successfully!')
        else:
            print('Website down!')
            msg = "error:container"

        print("Closing connection...")
        ssh.close()
        return msg

    except Exception as ex:
        print(f'Connection error: {ex}')
        print("Closing connection...")
        ssh.close()
        return "error:server"


# Create SG: Add personal ip
security_group_id = create_security_group('1.1.1.1')

# Create instances
create_instances(security_group_id)

# Installs Docker and Nginx in the instance
initialize_instance()

schedule.every(5).minutes.do(monitor_apps)

while True:
    schedule.run_pending()


