import boto3
from operator import itemgetter

ecr_client = boto3.client('ecr')

repositories = ecr_client.describe_repositories()

counter = 1
for repo in repositories['repositories']:
    print(f"\nRepo {counter}: {repo['repositoryName']}")
    counter += 1

    images = ecr_client.describe_images(
        repositoryName=repo['repositoryName']
    )

    images_sorted_by_date = sorted(images['imageDetails'], key=itemgetter('imagePushedAt'), reverse=True)

    counter_tag = 1
    for image in images_sorted_by_date:
        print(f"Tag {counter_tag}: {image['imageTags'][0]} (Pushed at {image['imagePushedAt']})")
        counter_tag += 1
