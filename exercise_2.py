import boto3
from operator import itemgetter

ec2_client = boto3.client('iam')

iam_users = ec2_client.list_users()
iam_users_sorted_by_date = sorted(iam_users['Users'], key=itemgetter('PasswordLastUsed'), reverse=True)

for user in iam_users['Users']:
    print(f"User: {user['UserName']}  User id: {user['UserId']}   Last active: {user['PasswordLastUsed']}")

print(f"\nThe last user to be active was {iam_users_sorted_by_date[0]['UserName']} (id: {iam_users_sorted_by_date[0]['UserId']})")
