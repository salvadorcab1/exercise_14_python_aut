import boto3

ec2_client = boto3.client('ec2', region_name="eu-west-3")

subnets = ec2_client.describe_subnets()

for subnet in subnets['Subnets']:
    print(subnet['SubnetId'])
